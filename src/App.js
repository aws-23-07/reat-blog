import React, { useState, useEffect } from'react';
import { BrowserRouter, Route, Routes, Switch } from'react-router-dom';
import Home from './Home';
import CreatePost from './CreatePost';
import EditPost from './EditPost';
import PostList from './PostList';

function App() {
  const [posts, setPosts] = useState([]);
  const [newPost, setNewPost] = useState({ title: '', content: '' });

  useEffect(() => {
    // Initialize posts array
    setPosts([
      { id: 1, title: 'Post 1', content: 'This is post 1' },
      { id: 2, title: 'Post 2', content: 'This is post 2' },
    ]);
  }, []);

  const handleCreatePost = (post) => {
    setPosts([...posts, post]);
  };

  const handleEditPost = (post) => {
    setPosts(posts.map((p) => (p.id === post.id? post : p)));
  };

  const handleDeletePost = (id) => {
    setPosts(posts.filter((p) => p.id!== id));
  };

  return (
    <BrowserRouter>
      Switch
      <Routes>
        <Route path="/" exact component={() => <Home posts={posts} />} />
        <Route path="/create" component={() => <CreatePost onCreatePost={handleCreatePost} />} />
        <Route path="/edit/:id" component={(props) => <EditPost {...props} onEditPost={handleEditPost} />} />
        <Route path="/posts" component={() => <PostList posts={posts} onDeletePost={handleDeletePost} />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;