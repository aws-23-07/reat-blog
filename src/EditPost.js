import React, { useState } from'react';
import { useParams } from'react-router-dom';
const posts =[]
const EditPost = ({ onEditPost, match }) => {
  const { id } = useParams();
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');

  useEffect(() => {
    const post = posts.find((p) => p.id === parseInt(id));
    if (post) {
      setTitle(post.title);
      setContent(post.content);
    }
  }, [id]);

  const handleSubmit = (e) => {
    e.preventDefault();
    onEditPost({ title, content, id: parseInt(id) });
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Title:
        <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
      </label>
      

      <label>
        Content:
        <textarea value={content} onChange={(e) => setContent(e.target.value)} />
      </label>
      

      <button type="submit">Update Post</button>
    </form>
  );
};

export default EditPost;