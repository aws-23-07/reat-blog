import React from'react';
import { Link } from'react-router-dom';

const Home = ({ posts }) => {
  return (
    <div>
      <h1>Blog Administration</h1>
      <p>
        <Link to="/create">Create a new post</Link>
      </p>
      <ul>
        {posts.map((post) => (
          <li key={post.id}>
            <Link to={`/edit/${post.id}`}>{post.title}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Home;