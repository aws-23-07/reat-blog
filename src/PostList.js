import React from'react';
import { Link } from'react-router-dom';

const PostList = ({ posts, onDeletePost }) => {
  return (
    <ul>
      {posts.map((post) => (
        <li key={post.id}>
          <Link to={`/edit/${post.id}`}>{post.title}</Link>
          <button onClick={() => onDeletePost(post.id)}>Delete</button>
        </li>
      ))}
    </ul>
  );
};

export default PostList;